vtxNG
=====

A Symfony project created on March 26, 2017, 6:06 pm.

vtxNG is our codename for the VoyaTrax Next Generation CMS. 

We will create a **FULL** *Blogging* system as a base system. It will have these
features:

  * Multilingual (i18n)
  * English & German included
  * Language schwitcher
  * Country Flags
  * Basic Standard pages
   * About Us
   * Disclaimer
   * Terms
   * Imprint
   * Privacy Policy
   * Contact
  * Bootstrap
  * JQuery
  * User login
  * User register
  * Admin area
  * EU Cookie Law conform
 
  
**more plans are being developed, for instance:**
  
  * Captcha / reCaptcha
  * Newsletter
  * RSS
  * Ability to get updates, like Bolt CM or WordPress
  
## LICENSE

[MIT License](LICENSE)


## Documentation

There is not a lot of docs yet. We are working on those as is needed. As far the building part goes, we will 
document that within the [wiki](wikis/home).


## Main Site

The [main website](http://www.voyatrax.tk) will be setup using vtxNG as soon as we are ready for testing.
This will happen, even if not ready for releasing.

